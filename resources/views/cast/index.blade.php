@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#cast_table").DataTable();
    });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush
@section('content')
<a href="/cast/create" class="btn btn-primary mb-5">Tambah</a>
<table class="table" id="cast_table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->bio}}</td>
            <td>
                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                <form action="/cast/{{$value->id}}" method="POST" style="display: inline">
                    @method('DELETE')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm my-1" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection