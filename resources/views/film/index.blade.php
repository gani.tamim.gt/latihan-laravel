@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#film_table").DataTable();
    });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush
@section('content')

@auth
<a href="/film/create" class="btn btn-primary mb-5">Tambah</a>
@endauth

@php
$i = 1
@endphp
@foreach ($film as $data)

@if ($i%3 == 1)
<div class="card-deck">
    @else
    @endif
    <div class="card text-center col-4">
        <img src="{{asset('images/'.$data->poster)}}" class="card-img-top" alt="Card image cap" style="height: 20vw">
        <div class="card-body">
            <span class="badge badge-info">{{$data->genre->genre}}</span>
            <h3>{{$data->judul}}</h3>
            <p class="card-text">{{Str::limit($data->ringkasan, 80)}}</p>
        </div>
        <div class="card-footer">
            @auth
            <form action="/film/{{$data->id}}" method="POST" class="d-inline">
                @csrf
                @method('DELETE')
                <div class="btn-group d-flex" role="group" aria-label="Basic example">
                    <a type="button" role="button" class="btn btn-info" href="/film/{{$data->id}}">Details</a>
                    <a type="button" role="button" class="btn btn-secondary" href="/film/{{$data->id}}/edit">Edit</a>
                    <input type="submit" class="btn btn-danger" value="Delete">
                </div>
            </form>
            @endauth

            @guest
            <div class="btn-group d-flex" role="group" aria-label="Basic example">
                <a type="button" role="button" class="btn btn-info" href="/film/{{$data->id}}">Details</a>
            </div>
            @endguest

            <small class="text-muted">Last updated at {{$data->updated_at}}</small>
        </div>
    </div>
    @if ($i%3 == 0 || $i == count($film))
</div>
@else
@endif
@php
$i++
@endphp
@endforeach
</div>
@endsection