@extends('layout.master')

@section('judul')
Halaman Create
@endsection

@section('content')
<div>
    <h2>Ubah Film</h2>
    <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">Judul Film</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul" value="{{$film->judul}}">
            @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="ringkasan">Ringkasan</label>
            <input type="text" class="form-control" name="ringkasan" id="ringkasan" placeholder="Masukkan ringkasan" value="{{$film->ringkasan}}">
            @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="number" class="form-control" name="tahun" id="tahun" placeholder="Masukkan tahun" value="{{$film->tahun}}">
            @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="genre">Genre</label>
            <select name="genre_id" id="genre_id" class="form-control">
                <option value="">---Pilih genre---</option>
                @foreach ($genre as $data)
                <option value="{{ $data->id}}" {{ $data->id === $film->genre_id ? 'selected' : ''}}>{{ $data->genre}}</option>
                @endforeach
            </select>
            @error('genre')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="poster">Poster</label>
            <input type="file" name="poster" class="form-control">
            @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Ubah</button>
    </form>
</div>
@endsection