@extends('layout.master')

@section('judul')
Halaman Show
@endsection

@section('content')
<img src="{{asset('images/'.$film->poster)}}">
<h1>{{$film->judul}}</h1>
<p>{{$film->ringkasan}}</p>

<h1>Isi Peran</h1>
<form action="/peran" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="peran">Peran</label>
        <input type="text" class="form-control" name="film_id" value="{{$film->id}}" id="film_id" placeholder="Masukkan peran" hidden>
        <input type="text" class="form-control" name="peran" id="peran" placeholder="Masukkan peran">
        @error('peran')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="cast">cast</label>
        <select name="cast_id" id="cast_id" class="form-control">
            <option value="">---Pilih cast---</option>
            @foreach ($cast as $data)
            <option value="{{ $data->id}}">{{ $data->nama}}</option>
            @endforeach
        </select>
        @error('cast')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

<a href="/film" class="btn btn-secondary">Back</a>
@endsection