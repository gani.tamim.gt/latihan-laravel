@extends('layout.master')

@section('judul')
Halaman Welcome
@endsection

@section('content')
    <p>
    <h1><b>Selamat Datang! {{$full_name}}</b></h1>
    </p>
    <p>
    <h3><b>Terima kasih telah bergabung di Website Kami.Media Belajar kita bersama!</b></h3>
    </p>
@endsection