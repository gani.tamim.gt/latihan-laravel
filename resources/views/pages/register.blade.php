@extends('layout.master')

@section('judul')
Halaman Register
@endsection

@section('content')
    <p>
    <h1><b>Buat Account Baru !</b></h1>
    </p>
    <p>
    <h2><b>Sign Up Form</b></h2>
    </p>
    <form action="/welcome" method="post">
        @csrf
        <p>First name:</p>
        <input type="text" name="first_name">
        <p>Last name:</p>
        <input type="text" name="last_name">
        <p>Gender:</p>
        <input type="radio" name="gender_radio" value="male"> Male
        <br>
        <input type="radio" name="gender_radio" value="female"> Female
        <br>
        <input type="radio" name="gender_radio" value="other"> Other
        <!-- gender just male,female only.So i dont about other? -->
        <p>Nationality:</p>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox" name="lang1" value="indonesian"> Bahasa Indonesia
        <br>
        <input type="checkbox" name="lang2" value="english"> English
        <br>
        <input type="checkbox" name="lang3" value="other"> Other
        <p>Bio:</p>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
    </form>

    @endsection