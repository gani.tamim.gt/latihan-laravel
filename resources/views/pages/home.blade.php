@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <p>
    <h1><b>Media Online</b></h1>
    </p>
    <p>
    <h2><b>Sosial Media Developer</b></h2>
    </p>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <p>
    <h2><b>Benefit Join di Media Online</b></h2>
    </p>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <p>
    <h2><b>Cara Bergabung ke Media Online</b></h2>
    </p>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection