@extends('layout.master')

@section('judul')
Halaman Update
@endsection

@section('content')
<div>
    <h2>Ubah Profile</h2>
    <form action="/profile/{{$profile->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan name" value="{{$profile->user->name}}" disabled>
            @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan umur" value="{{$profile->umur}}">
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan bio">{{$profile->bio}}</textarea>
            @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea type="number" class="form-control" name="alamat" id="alamat" placeholder="Masukkan alamat">{{$profile->alamat}}</textarea>
            @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" email="email" id="email" placeholder="Masukkan email" value="{{$profile->user->email}}" disabled>
            @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Ubah</button>
    </form>
</div>
@endsection