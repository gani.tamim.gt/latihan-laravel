@extends('layout.master')

@section('judul')
Halaman Create
@endsection

@section('content')
<div>
    <h2>Tambah Data</h2>
    <form action="/genre" method="POST">
        @csrf
        <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" class="form-control" name="genre" id="genre" placeholder="Masukkan genre">
            @error('genre')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection