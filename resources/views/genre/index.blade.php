@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#genre_table").DataTable();
    });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush
@section('content')
<a href="/genre/create" class="btn btn-primary mb-5">Tambah</a>
<table class="table" id="genre_table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Genre</th>
            <th scope="col">Daftar Film</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->genre}}</td>
            <td>
                <ul>
                    @foreach ($value->film as $data)
                    <li>{{$data->judul}}</li>
                    @endforeach
                </ul>
            </td>
            <td>
                <a href="/genre/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                <a href="/genre/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                <form action="/genre/{{$value->id}}" method="POST" style="display: inline">
                    @method('DELETE')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm my-1" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection