@extends('layout.master')

@section('judul')
Halaman Edit
@endsection

@section('content')
<div>
    <h2>Edit genre {{$genre->id}}</h2>
    <form action="/genre/{{$genre->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" class="form-control" name="genre" value="{{$genre->genre}}" id="genre" placeholder="Masukkan genre">
            @error('genre')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection