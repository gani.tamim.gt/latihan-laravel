<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = "peran";
    protected $fillable = ["film_id", "cast_id", "jenis_peran"];


    public function film()
    {
        return $this->belongsToMany('App\Film');
    }
    public function cast()
    {
        return $this->belongsToMany('App\Cast');
    }
}
