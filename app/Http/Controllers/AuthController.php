<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('pages.register');
    }

    public function welcome(Request $request)
    {
        $full_name = $request['first_name'] . " " . $request['last_name'];

        return view('pages.welcome', compact('full_name'));
    }
}
