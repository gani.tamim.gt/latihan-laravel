<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Peran;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index()
    {
        $peran = Peran::all();
        return view('peran.index', compact('peran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $peran = DB::table('peran')->get();
        return view('peran.create', compact('peran'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'film_id' => 'required',
            'cast_id' => 'required',
            'peran' => 'required',
        ]);


        Peran::create([
            "film_id" => $request["film_id"],
            "cast_id" => $request["cast_id"],
            "jenis_peran" => $request["peran"],
        ]);
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::find($id);
        return view('peran.show', compact('peran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $peran = Peran::find($id);
        return view('peran.edit', compact('peran', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $peran = Peran::find($id);
        if ($request->has('poster')) {
            $posterName = time() . "." . $request->poster->extension();
            $request->poster->move(public_path('images'), $posterName);
            $image_path = public_path('images') . '/' . $peran->poster;
            unlink($image_path);
        } else {
            $posterName = $peran->poster;
        }

        $peran->update([
            "judul" => $request["judul"],
            "ringkasan" => $request["ringkasan"],
            "tahun" => $request["tahun"],
            "genre_id" => $request["genre_id"],
            "poster" => $posterName,
        ]);
        return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peran = Peran::find($id);
        $image_path = public_path('images') . '/' . $peran->poster;
        unlink($image_path);
        $peran->delte();
        return redirect('/peran');
    }
}
