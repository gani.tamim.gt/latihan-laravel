<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Genre;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'genre' => 'required',
        ]);
        Genre::create([
            "genre" => $request["genre"],
        ]);
        return redirect('/genre');
    }

    public function index()
    {
        $genre = Genre::with('film')->get();
        return view('genre.index', compact('genre'));
    }

    public function show($id)
    {
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
    }

    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('genre.edit', compact('genre'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'genre' => 'required',
        ]);

        Genre::find($id)
            ->update([
                "genre" => $request["genre"],
            ]);
        return redirect('/genre');
    }

    public function destroy($id)
    {
        Genre::find($id)->delete();
        return redirect('/genre');
    }
}
