<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Film;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genre')->get();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $posterName = time() . "." . $request->poster->extension();
        $request->poster->move(public_path('images'), $posterName);

        Film::create([
            "judul" => $request["judul"],
            "ringkasan" => $request["ringkasan"],
            "tahun" => $request["tahun"],
            "genre_id" => $request["genre_id"],
            "poster" => $posterName,
        ]);
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = DB::table('cast')->get();
        $film = Film::find($id);
        return view('film.show', compact('film', 'cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $film = Film::find($id);
        return view('film.edit', compact('film', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $film = Film::find($id);
        if ($request->has('poster')) {
            $posterName = time() . "." . $request->poster->extension();
            $request->poster->move(public_path('images'), $posterName);
            $image_path = public_path('images') . '/' . $film->poster;
            unlink($image_path);
        } else {
            $posterName = $film->poster;
        }

        $film->update([
            "judul" => $request["judul"],
            "ringkasan" => $request["ringkasan"],
            "tahun" => $request["tahun"],
            "genre_id" => $request["genre_id"],
            "poster" => $posterName,
        ]);
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
        $image_path = public_path('images') . '/' . $film->poster;
        unlink($image_path);
        $film->delte();
        return redirect('/film');
    }
}
